import sys

class Vertex:
    """
    Represents a vertex in a graph.

    Args:
        node (str): The identifier of the vertex.
        data: The data associated with the vertex.

    Attributes:
        id (str): The identifier of the vertex.
        data: The data associated with the vertex.
        adjacent (dict): A dictionary of adjacent vertices and their edge weights.
        distance (int): The distance from the starting vertex (used in graph algorithms).
        visited (bool): A flag to mark whether the vertex has been visited.
        previous: A reference to the previous vertex in the path (used in graph algorithms).

    Methods:
        set_data(data): Set the data associated with the vertex.
        add_neighbor(neighbor, weight=0): Add a neighboring vertex with an optional edge weight.
        get_connections(): Get a list of neighboring vertices.
        get_id(): Get the identifier of the vertex.
        get_data(): Get the data associated with the vertex.
        get_weight(neighbor): Get the edge weight to a neighboring vertex.
        set_distance(dist): Set the distance attribute.
        get_distance(): Get the distance attribute.
        set_previous(prev): Set the previous attribute.
        set_visited(): Mark the vertex as visited.
        __str__(): Return a string representation of the vertex.
        __lt__(other): Compare vertices based on their distances (used in graph algorithms).
    """
    def __init__(self, node, data):
        """
        Initialize a vertex with a unique node ID and optional data.

        Args:
            node: The unique identifier for the vertex (node).
            data: Optional data associated with the vertex.
        """
        self.id = node
        self.data = data
        self.adjacent = {}
        # Set distance to infinity for all nodes
        self.distance = sys.maxsize
        # Mark all nodes unvisited        
        self.visited = False  
        # Predecessor
        self.previous = None

    def set_data(self,data):
        """
        Set the data associated with the vertex.

        Args:
            data: The data to set for the vertex.

        Example:
            vertex.set_data(42)
        """
        self.data = data

    def add_neighbor(self, neighbor, weight=0):
        """
        Add a neighbor vertex with an optional edge weight.

        Args:
            neighbor: The neighboring vertex to add.
            weight (int): The edge weight (default is 0).

        Example:
            vertex.add_neighbor(neighbor_vertex, 5)
        """
        self.adjacent[neighbor] = int(weight)

    def get_connections(self):
        """
        Get a list of neighboring vertices.

        Returns:
            list: A list of neighboring vertices.

        Example:
            neighbors = vertex.get_connections()
        """
        return self.adjacent.keys()  

    def get_id(self):
        """
        Get the node ID of the vertex.

        Returns:
            Any: The node ID of the vertex.

        Example:
            node_id = vertex.get_id()
        """
        return self.id
    
    def get_data(self):
        """
        Get the data associated with the vertex.

        Returns:
            Any: The data associated with the vertex.

        Example:
            data = vertex.get_data()
        """
        return self.data

    def get_weight(self, neighbor):
        """
        Get the edge weight to a neighboring vertex.

        Args:
            neighbor: The neighboring vertex to get the edge weight for.

        Returns:
            int: The edge weight.

        Example:
            weight = vertex.get_weight(neighbor_vertex)
        """
        return self.adjacent[neighbor]

    def set_distance(self, dist):
        """
        Set the distance attribute of the vertex.

        Args:
            dist (int): The distance value to set.

        Example:
            vertex.set_distance(10)
        """
        self.distance = dist

    def get_distance(self):
        """
        Get the distance attribute of the vertex.

        Returns:
            int: The distance value.

        Example:
            distance = vertex.get_distance()
        """
        return self.distance

    def set_previous(self, prev):
        """
        Set the previous vertex attribute for internal use.

        Args:
            prev (Vertex): The previous vertex.

        Example:
            vertex.set_previous(previous_vertex)
        """
        self.previous = prev

    def set_visited(self):
        """
        Set the visited attribute of the vertex to True.

        Example:
            vertex.set_visited()
        """
        self.visited = True

    def __str__(self):
        return str(self.id) + ' adjacent: ' + str([x.id for x in self.adjacent])

    def __lt__(self, other):
        return self.distance < other.distance

class Graph:
    """
    A class representing a graph.

    Attributes:
        vert_dict (dict): A dictionary mapping node IDs to their corresponding vertices.
        num_vertices (int): The total number of vertices in the graph.
        previous (Vertex): A reference to the previous vertex for internal use.

    Methods:
        __init__(self): Initializes an empty graph.
        add_vertex(self, node, data=None): Adds a new vertex to the graph.
        get_vertex(self, v): Retrieves a vertex by its node ID.
        add_edge(self, frm, to, cost=0): Adds an edge between two vertices in the graph.
        get_vertices(self): Returns a list of node IDs for all vertices in the graph.
        set_previous(self, current): Sets the previous vertex for a given current vertex.
        get_previous(self, current): Retrieves the previous vertex for a given current vertex.
    """
    def __init__(self):
        """
        Allow iteration over the vertices in the graph.

        Yields:
            Vertex: The next vertex in the graph during iteration.
        """
        self.vert_dict = {}
        self.num_vertices = 0
        self.previous = None

    def __iter__(self):
        return iter(self.vert_dict.values())

    def add_vertex(self, node, data):
        """
        Add a new vertex to the graph.

        Args:
            node: The unique identifier for the vertex (node).
            data: Optional data associated with the vertex.

        Returns:
            Vertex: The newly added vertex.
        
        Example:
            g = Graph()
            vertex = g.add_vertex('a', 42)
        """
        self.num_vertices = self.num_vertices + 1
        new_vertex = Vertex(node, data)  # Pass both node and data here
        self.vert_dict[node] = new_vertex
        return new_vertex

    def get_vertex(self, v ):
        """
        Retrieve a vertex by its node ID.

        Args:
            v: The node ID of the vertex to retrieve.

        Returns:
            Vertex or None: The vertex if found, or None if not found.
        
        Example:
            g = Graph()
            vertex = g.get_vertex('a')
        """
        if v in self.vert_dict:
            return self.vert_dict[v]
        else:
            return None

    def add_edge(self, frm, to, cost = 0):
        """
        Add an edge between two vertices in the graph.

        Args:
            frm: The node ID of the source vertex.
            to: The node ID of the destination vertex.
            cost (int): The cost or weight of the edge (default is 0).
        
        Example:
            g = Graph()
            g.add_edge('a', 'b', 5)
        """
        if frm not in self.vert_dict:
            self.add_vertex(frm, None)  # You can pass None as data or provide data here
        if to not in self.vert_dict:
            self.add_vertex(to, None)  # You can pass None as data or provide data here

        self.vert_dict[frm].add_neighbor(self.vert_dict[to], cost)
        self.vert_dict[to].add_neighbor(self.vert_dict[frm], cost)

    def get_vertices(self):
        """
        Get a list of node IDs for all vertices in the graph.

        Returns:
            list: A list of node IDs.
        
        Example:
            g = Graph()
            nodes = g.get_vertices()
        """
        print(self.vert_dict.keys())
        return self.vert_dict.keys()

    def set_previous(self, current):
        """
        Set the previous vertex for internal use.

        Args:
            current (Vertex): The previous vertex to set.

        Example:
            g = Graph()
            vertex = g.get_vertex('a')
            g.set_previous(vertex)
        """
        self.previous = current

    def get_previous(self):
        """
        Get the previous vertex for internal use.

        Returns:
            Vertex: The previous vertex.

        Example:
            g = Graph()
            previous_vertex = g.get_previous()
        """
        return self.previous

def shortest(v, path):
    ''' make shortest path from v.previous'''
    if v.previous:
        path.append(v.previous.get_id())
        shortest(v.previous, path)
    return

import heapq

def dijkstra(aGraph, start):
    """
    Calculate the shortest path using Dijkstra's algorithm.

    Args:
        aGraph (Graph): The graph to perform Dijkstra's algorithm on.
        start (Vertex): The starting vertex for the algorithm.

    Returns:
        None

    Note:
        This function updates the distances and predecessors of vertices in the given graph.

    Example:
        g = Graph()
        start_vertex = g.get_vertex('a')
        dijkstra(g, start_vertex)
    """
    print("Dijkstra's shortest path")
    # Set the distance for the start node to zero 
    start.set_distance(0)

    # Put tuple pair into the priority queue
    unvisited_queue = [(v.get_distance(),v) for v in aGraph]
    heapq.heapify(unvisited_queue)

    while len(unvisited_queue):
        # Pops a vertex with the smallest distance 
        uv = heapq.heappop(unvisited_queue)
        current = uv[1]
        current.set_visited()

        #for next in v.adjacent:
        for next in current.adjacent:
            # if visited, skip
            if next.visited:
                continue
            new_dist = current.get_distance() + current.get_weight(next)
            
            if new_dist < next.get_distance():
                next.set_distance(new_dist)
                next.set_previous(current)
                print (f'|| updated : current = {current.get_id()} | next = {next.get_id()} | new_dist = {next.get_distance()}')
            else:
                print (f'|| not updated : current = {current.get_id()} | next = {next.get_id()} | new_dist = {next.get_distance()}')

        # Rebuild heap
        # 1. Pop every item
        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)
        # 2. Put all vertices not visited into the queue
        unvisited_queue = [(v.get_distance(),v) for v in aGraph if not v.visited]
        heapq.heapify(unvisited_queue)

if __name__ == '__main__':
    g = Graph()
