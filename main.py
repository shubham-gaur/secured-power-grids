#!/usr/bin/env python3.10
'''
    A spanning tree rooting at the collector device.            /
    Each node collects data from its children, aggregates from  /
    its own data and sends the intermediate result to the parent/
    node.                                                       /
'''

import sys
import spinner
import feed
import encrypt

if __name__ == '__main__':
    gi = feed.GraphInput()
    gi.input_data()
    gi.calc_data()
    sys.stdout.write('\nInitializing ')
    spinner.spin(' : Completed!')

    while True:
        print('\n\n==================================Commands==================================')
        print('To see tree                   "t   "')
        print('To start                      "s   "')
        print('To print destinations         "des "')
        print('To encrypt                    "e   "')
        print('To see encrypted list enter   "el  "')
        print('To see encrypted result enter "er  "')
        print('To see collector data enter   "cd  "')
        print('To decrypt result enter       "d   "')
        print('To quit press                 "q   "')
        print('==================================Commands==================================\n\n')

        x = input("\nInsert value and press Enter : ")
        match x:
            case 'des':
                gi.destinations()
            case 's':
                gi.implement()
                gi.path()
            case 'e':
                gi.encrypt()
            case 'el':
                encrypt.get_list()
            case 'er':
                encrypt.enc_res()
            case 'cd':
                gi.src_data()
            case 'd':
                encrypt.decrypt()
            case 'q':
                break
            case _:
                print('No valid input')