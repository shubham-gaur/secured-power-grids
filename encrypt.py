import sys
import spinner
import feed
import encryption as paillier_enc

enc_list = []
priv, pub = paillier_enc.generate_keypair(512)
cz = paillier_enc.encrypt(pub, 0)

def enc(path, graph):
    """
    Encrypt data associated with nodes in a path.

    Args:
        path (list): A list of node IDs representing the path.

    Returns:
        None

    Example:
        enc(['a', 'b', 'c'])  # Encrypt data associated with nodes 'a', 'b', 'c'.
    """
    global cz
    for t in path[:-1]:
        target = graph.get_vertex(t)
        if target is not None:
            dat = target.get_data()
            cx = paillier_enc.encrypt(pub, dat)
            enc_list.append(cx)
            sys.stdout.write('Encrypting node %s ' % (t))
            spinner.spin('<--> Encrypted!')
            cz = paillier_enc.e_add(pub, cz, cx)
        else:
            print("Error: Vertex '%s' not found in the graph.")
            # Handle the error, e.g., by skipping or logging

def set_data(graph):
    """
    Set data associated with a specific vertex.

    Returns:
        None

    Example:
        setData()  # Set data for a vertex.
    """
    target = graph.get_vertex('a')
    if target is not None:
        target.set_data(cz)
    else:
        print("Error: Vertex 'a' not found in the graph.")
        # Handle the error, e.g., by skipping or logging

def enc_res():
    """
    Fetch and display encrypted results.

    Returns:
        None

    Example:
        encRes()  # Fetch and display encrypted results.
    """
    sys.stdout.write('Fetching results ')
    spinner.spin('... Done!')
    print('\n#   %s' % cz)

def decrypt():
    """
    Decrypt encrypted data.

    Returns:
        None

    Example:
        dec()  # Decrypt encrypted data.
    """
    z = paillier_enc.decrypt(priv, pub, cz)
    sys.stdout.write("\nDecrypting ")
    spinner.spin('<--> Decrypted : # %s' % z)

def get_list():
    """
    Get and display a list of encrypted data.

    Returns:
        None

    Example:
        getList()  # Get and display a list of encrypted data.
    """
    print(enc_list[::])
