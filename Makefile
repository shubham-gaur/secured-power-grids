SHELL=/bin/bash
VER="$(shell python --version)"
MY_PYTHON="$(shell which python)"
APP="Secured-Smart-Grid"
PREFIX="[${APP}\|${VER}]::"

all: build test dist

.PHONY: check
check:
	@echo "${PREFIX} Checking python status..."
	@if [[ ${VER} == "Python 3.10."* ]]; then\
        echo "${PREFIX} Success: Version satisfied";\
	else\
		echo "${PREFIX} Failed: Python Version found ${VER} but supported 3.10+";\
		exit 1;\
    fi
	@if [[ "$(PWD)/.venv/bin/python" == "${MY_PYTHON}" ]]; then\
        echo "${PREFIX} Success: Active virtual environment found";\
	else\
		echo "${PREFIX} Failed: Please switch to virtual environment \"$(PWD)/.venv/bin/python\"";\
		echo "${PREFIX} Something like: source \"$(PWD)/.venv/bin/activate\"";\
		exit 1;\
    fi

.PHONY: build
build:
	@if [ ! -d ".venv" ]; then \
		echo "${PREFIX} Setting up environment..."; \
		echo "${PREFIX} Creating virtual environment..."; \
		python -m venv .venv; \
		echo "${PREFIX} Switching to virtual environment..."; \
		source ./.venv/bin/activate; \
		if [ -f "requirements.txt" ]; then \
			echo "${PREFIX} Installing requirements..."; \
			pip install -r requirements.txt; \
		else \
			echo "${PREFIX} requirements.txt file not found, skipping installation."; \
		fi; \
		echo "${PREFIX} Setup completed!!!"; \
		echo "${PREFIX} Created virtual env: \"$(PWD)/.venv/bin/python\""; \
		echo "${PREFIX} Activate virtual env using:"; \
		echo "${PREFIX}   - source \"$(PWD)/.venv/bin/activate\""; \
	else \
		echo "${PREFIX} Virtual environment exists!"; \
	fi

.PHONY: run
run: help check
	@echo "${PREFIX} Initializing main..."
	@python main.py
	@echo "${PREFIX} Completed!!!"

.PHONY: dist
dist: help check
	python -m nuitka --follow-imports --standalone main.py
	@echo "${PREFIX} Distribution complete!!!"

.PHONY: clean
clean: help
	@rm -rf *.dist *.build *.pyc
	@echo "${PREFIX} Deleted distribution files successfully!!!"
	@find . -type d -name  "__pycache__" -exec rm -r {} +
	@echo "${PREFIX} Deleted __pycache__ successfully!!!"

help:
	@echo -e "\n\t***** ${PREFIX} *****"
	@echo "1.To build development environment: make build"
	@echo "2.To make tests:                    make test"
	@echo "3.To generate C distribution:       make dist"
	@echo "  To execute 1-3:                   make all"
	@echo "4.To run source code:               make run"
	@echo "5.To clean temporary files:         make clean"
	@echo -e "\t***** ${PREFIX} *****\n"
