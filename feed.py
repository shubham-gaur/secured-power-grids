import sys
import time
import csv
import math
import graph
import spinner
import encrypt

class GraphInput:
    """
    A class for handling input self.data, processing the graph, and performing various operations on it.
    """
    
    def __init__(self) -> None:
        test1 = open("vertex.csv", "r", encoding="utf-8")
        test2 = open("edge.csv", "r", encoding="utf-8")
        # Set up CSV reader and process the header
        self.csv_reader_1 = csv.reader(test1)
        self.csv_reader_2 = csv.reader(test2)

        self.header_1 = next(self.csv_reader_1)
        self.header_2 = next(self.csv_reader_2)
        self.node_index = self.header_1.index("Node")
        self.head_data = self.header_1.index("Data")
        self.x_index = self.header_1.index("X")
        self.y_index = self.header_1.index("Y")

        # Make empty lists
        self.data = []
        self.x_list = []
        self.y_list = []
        self.node_list = []
        self.meters = []

        # Graph Initialization
        self.graph = graph.Graph()
    
    def input_data(self):
        """
        Input self.data from CSV files and populate the graph with vertices.

        Returns:
            None
        """
        for row in self.csv_reader_1:
            node = row[self.node_index]
            datum = row[self.head_data]
            x = row[self.x_index]
            y = row[self.y_index]
            self.node_list.append(node)
            self.data.append(int(datum))
            self.x_list.append(int(x))
            self.y_list.append(int(y))
            self.graph.add_vertex(node, int(datum))

    def calc_data(self):
        """
        Calculate edge weights and add edges to the graph.

        Returns:
            None
        """
        main = dict(zip(self.node_list, zip(self.x_list, self.y_list)))
        for row in self.csv_reader_2:
            Src, Des, Rest = row[:3] + [None] * (3 - len(row))
            result_list1 = list(main[Src])
            result_list2 = list(main[Des])
            ed = math.sqrt(
                ((int(result_list1[0]) - int(result_list2[0])) ** 2) +
                ((int(result_list1[1]) - int(result_list2[1])) ** 2))
            self.graph.add_edge(Src, Des, int(ed))

    def destinations(self):
        """
        Input destination self.data.

        Returns:
            None
        """
        dest = input("Enter destination : ")
        self.meters.append(str(dest))

        print('\n\n-------------------------------------------')
        print(f'self.meters are : {self.meters[::]}')
        print('-------------------------------------------\n\n')

    # def tree(self):
    #     sys.stdout.write('\nTree self.data: Fetching ')
    #     spin.Spinner(' <--> Fetched!')
    #     t =  Tree("((a,(e,b,(d,c,(f)))));")
    #     print(" _______________________________________")
    #     print("|                                       |")
    #     print t
    #     print("|_______________________________________|")
    #     time.sleep(3)

    def implement(self):
        """
        Implement the graph self.data.

        Returns:
            None
        """
        sys.stdout.write('\nGraph self.data: Fetching ')
        spinner.spin(' <--> Fetched!')
        print(" _________________________________________")
        print('|                                         |')
        for v in self.graph:
            for w in v.get_connections():
                vid = v.get_id()
                wid = w.get_id()
                dat1 = ('{: ^3}'.format(v.get_data()))
                dat2 = ('{: ^3}'.format(w.get_data()))
                dis = ('{: ^3}'.format(v.get_weight(w)))
                print(f'| Src {vid} = {dat1} | Dest {wid} = {dat2} | Cost = {dis} | ')

        print("|_________________________________________|\n")
        time.sleep(3)

    def path(self):
        """
        Find the shortest path in the graph.

        Returns:
            None
        """
        graph.dijkstra(self.graph, self.graph.get_vertex('a'))

    def encrypt(self):
        """
        Encrypt self.data for each destination.

        Returns:
            None
        """
        for t in self.meters:
            target = self.graph.get_vertex(t)
            if target is not None:
                path = [t]
                graph.shortest(target, path)
                sys.stdout.write(f'Fetching shortest path for Meter {t}')
                spinner.spin('... Done!')
                print(f'The shortest path for {t} : {path[::]}\n')
                encrypt.enc(path, self.graph)
                print(f'\nDestination {path[0]} completed-------------------------\n')
            else:
                print("Error: Vertex '%s' not found in the graph.")
                # Handle the error, e.self.graph., by skipping or logging
        encrypt.set_data(self.graph)

    def src_data(self):
        """
        Get source self.data.

        Returns:
            None
        """
        sys.stdout.write('Fetching source self.data ')
        spinner.spin('... Done!')
        target = self.graph.get_vertex('a')
        if target is not None:
            print(target.get_data())
        else:
            print("Error: Vertex 'a' not found in the graph.")
            # Handle the error, e.self.graph., by skipping or logging

    def get_data(self):
        """
        Get self.data from the graph and update the vertex.

        Returns:
            None
        """
        target = self.graph.get_vertex('a')
        if target is not None:
            target.set_data(encrypt.cz)
        else:
            print("Error: Vertex 'a' not found in the graph.")
            # Handle the error, e.self.graph., by skipping or logging
    
    def get_vertex(self, node):
        return self.graph.get_vertex(node)

if __name__ == '__main__':
    gi = GraphInput()
