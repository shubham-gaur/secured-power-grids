import sys
import time
import itertools

spinner1 = itertools.cycle(['   ', '>  ', '>> ', '>>>'])
spinner2 = itertools.cycle(['.  ', '.. ', '...'])

def spin(txt):
    """
    Display a spinning animation in the console.

    Args:
        txt (str): The text message to display alongside the spinning animation.

    Returns:
        None

    Example:
        Spinner("Processing data")  # Display a spinning animation with the message "Processing data".
    """
    text = txt
    for _ in range(30):
        sys.stdout.write('%s' % next(spinner1))
        sys.stdout.write('\b\b\b')
        time.sleep(0.07)
        sys.stdout.flush()
    spinner_stop(text)

def spinner_stop(txt):
    """
    Stop the spinning animation and display a final message in the console.

    Args:
        txt (str): The final message to display after stopping the spinning animation.

    Returns:
        None

    Example:
        spinner_stop("Process completed")  # Stop the spinner and display "Process completed".
    """
    # Function code remains the same
    for _ in range(3):
        sys.stdout.write('%s' % next(spinner2))
        sys.stdout.write('\b\b\b')
        time.sleep(0.5)
        sys.stdout.flush()
    print(f"{txt}")
