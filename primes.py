import random

def ipow(base, exponent, modulus):
    """Calculates (base**exponent) % modulus via binary exponentiation, yielding intermediate
       results as Rabin-Miller requires"""
    A = base = int(base % modulus)
    yield A
    t = 1
    while t <= exponent:
        t <<= 1

    # t = 2**k, and t > exponent
    t >>= 2
    
    while t:
        A = (A * A) % modulus
        if t & exponent:
            A = (A * base) % modulus
        yield A
        t >>= 1


def rabin_miller_witness(test, possible):
    """Using Rabin-Miller witness test, will return True if possible is
       definitely not prime (composite), False if it may be prime."""    
    return 1 not in ipow(test, possible-1, possible)

smallprimes = (2,3,5,7,11,13,17,19,23,29,31,37,41,43,
               47,53,59,61,67,71,73,79,83,89,97)

def default_k(bits):
    """
    Calculate the default number of iterations for the Rabin-Miller test based on the number of bits.

    Args:
        bits (int): The number of bits in the integer to be tested.

    Returns:
        int: The default number of iterations for the Rabin-Miller test.

    Example:
        k = default_k(512)  # Calculate the default number of iterations for 512-bit integers.
    """
    return max(40, 2 * bits)


def is_probably_prime(possible, k=None):
    """
    Check if a number is probably prime using the Rabin-Miller algorithm.

    Args:
        possible (int): The number to check for primality.
        k (int, optional): The number of iterations for the Rabin-Miller test. Default is None.

    Returns:
        bool: True if the number is probably prime, False otherwise.

    Example:
        is_prime = is_probably_prime(17)  # Check if 17 is probably prime.
    """
    if possible == 1:
        return True
    if k is None:
        k = default_k(possible.bit_length())
    for i in smallprimes:
        if possible == i:
            return True
        if possible % i == 0:
            return False
    for i in range(k):
        test = random.randrange(2, possible - 1) | 1
        if rabin_miller_witness(test, possible):
            return False
    return True

def generate_prime(bits, k=None):
    """Will generate an integer of b bits that is probably prime 
       (after k trials). Reasonably fast on current hardware for 
       values of up to around 512 bits."""    
    assert bits >= 8

    if k is None:
        k = default_k(bits)

    while True:
        possible = random.randrange(2 ** (bits-1) + 1, 2 ** bits) | 1
        if is_probably_prime(possible, k):
            return possible
