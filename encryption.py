import math
import primes

def invmod(a, p, maxiter=1000000):
    """The multiplicative inverse of a in the integers modulo p:
         a * b == 1 mod p
       Returns b.
       (http://code.activestate.com/recipes/576737-inverse-modulo-p/)"""
    if a == 0:
        raise ValueError(f'0 has no inverse mod {p}')
    r = a
    d = 1
    for i in range(min(p, maxiter)):
        d = ((p // r + 1) * d) % p
        r = (d * a) % p
        if r == 1:
            break
    else:
        raise ValueError(f'{a} has no inverse mod {p}')
    return d

def modpow(base, exponent, modulus):
    """Modular exponent:
         c = b ^ e mod m
       Returns c.
       (http://www.programmish.com/?p=34)"""
    result = 1
    while exponent > 0:
        if exponent & 1 == 1:
            result = (result * base) % modulus
        exponent = exponent >> 1
        base = (base * base) % modulus
    return result

class PrivateKey(object):
    """
    Represents a private key for a cryptographic system.

    Attributes:
        l (int): The private key parameter l.
        m (int): The private key parameter m.

    Args:
        p (int): The first prime number.
        q (int): The second prime number.
        n (int): The modulus for the private key.
    """
    def __init__(self, p, q, n):
        self.l = (p - 1) * (q - 1)
        self.m = invmod(self.l, n)

    def __repr__(self):
        return f'<PrivateKey:  {self.l} {self.m} >'

class PublicKey(object):
    """
    Represents a public key for a cryptographic system.

    Attributes:
        n (int): The modulus for the public key.
        n_sq (int): The square of the modulus.
        g (int): The public key parameter g.

    Args:
        n (int): The modulus for the public key.
    """

    @classmethod
    def from_n(cls, n):
        return cls(n)

    def __init__(self, n):
        self.n = n
        self.n_sq = n * n
        self.g = n + 1

    def __repr__(self):
        return f'<PublicKey: {self.n}>'

def generate_keypair(bits):
    """
    Generate a key pair for a cryptographic system.

    Args:
        bits (int): The number of bits for the key size.

    Returns:
        tuple: A tuple containing a PrivateKey and PublicKey.

    Example:
        private_key, public_key = generate_keypair(2048)
    """
    p = primes.generate_prime(bits // 2)
    q = primes.generate_prime(bits // 2)
    n = p * q
    return PrivateKey(p, q, n), PublicKey(n)

def encrypt(pub, plain):
    """
    Encrypt a plaintext using a public key.

    Args:
        pub (PublicKey): The public key for encryption.
        plain (int): The plaintext to encrypt.

    Returns:
        int: The encrypted ciphertext.

    Example:
        ciphertext = encrypt(public_key, 42)
    """
    while True:
        r = primes.generate_prime(int(round(math.log(pub.n, 2))))
        if 0 < r < pub.n:
            break
    x = pow(r, pub.n, pub.n_sq)
    cipher = (pow(pub.g, plain, pub.n_sq) * x) % pub.n_sq
    return cipher

def decrypt(priv, pub, cipher):
    """
    Decrypt a ciphertext using a private key and public key.

    Args:
        priv (PrivateKey): The private key for decryption.
        pub (PublicKey): The public key for decryption.
        cipher (int): The ciphertext to decrypt.

    Returns:
        int: The decrypted plaintext.

    Example:
        plaintext = decrypt(private_key, public_key, ciphertext)
    """
    x = pow(cipher, priv.l, pub.n_sq) - 1
    plain = ((x // pub.n) * priv.m) % pub.n
    return plain

def e_add(pub, a, b):
    """Add one encrypted integer to another"""
    return a * b % pub.n_sq

def e_add_const(pub, a, n):
    """Add constant n to an encrypted integer"""
    return a * modpow(pub.g, n, pub.n_sq) % pub.n_sq

def e_mul_const(pub, a, n):
    """Multiplies an encrypted integer by a constant"""
    return modpow(a, n, pub.n_sq)
